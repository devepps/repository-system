package repository.data.generator;

import java.lang.reflect.Field;
import java.util.Random;

import collection.sync.SyncHashMap;

@GeneratorKey()
public class IntegerGenerator extends Generator<Integer> {

	private final Random random = new Random();
	
	public IntegerGenerator(Field field) {
		super(field);
		this.field.setAccessible(true);
	}

	@Override
	public Integer generate(Object entity) {
		int generated = random.nextInt();
		try {
			field.set(entity, generated);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return generated;
	}

	@Override
	public Integer generateUnique(Object entity, SyncHashMap<Object, ?> dataBase) {
		for (int i = 0; i < maxTries; i++) {
			int generatedObject = random.nextInt();
			int status = dataBase.getSyncManager().syncronize(() -> {
				for (Object content : dataBase.values()) {
					try {
						int existingObject = field.getInt(content);
						if (existingObject == generatedObject) {
							return 1;
						}
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
						return -1;
					}
				}
				return 0;
			});
			if (status == 0) {
				try {
					field.set(entity, generatedObject);
				} catch (IllegalArgumentException | IllegalAccessException e) {
				}
				return generatedObject;
			} else if (status == -1) {
				return null;
			}
		}
		return null;
	}

}
