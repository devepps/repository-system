package repository.data.generator;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(TYPE)
public @interface GeneratorKey {
	
	public String maxTriesKey() default "Repository.Generator.Max_Generate_Unique_Tries";

}
