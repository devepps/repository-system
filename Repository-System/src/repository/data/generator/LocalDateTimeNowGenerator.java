package repository.data.generator;

import java.lang.reflect.Field;
import java.time.LocalDateTime;

import collection.sync.SyncHashMap;

@GeneratorKey()
public class LocalDateTimeNowGenerator extends Generator<LocalDateTime> {
	
	public LocalDateTimeNowGenerator(Field field) {
		super(field);
		this.field.setAccessible(true);
	}

	@Override
	public LocalDateTime generate(Object entity) {
		LocalDateTime generated = LocalDateTime.now();
		try {
			field.set(entity, generated);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return generated;
	}

	@Override
	public LocalDateTime generateUnique(Object entity, SyncHashMap<Object, ?> dataBase) {
		for (int i = 0; i < maxTries; i++) {
			LocalDateTime generatedObject = LocalDateTime.now();
			int status = dataBase.getSyncManager().syncronize(() -> {
				for (Object content : dataBase.values()) {
					try {
						LocalDateTime existingObject = (LocalDateTime) field.get(content);
						if (existingObject.isEqual(generatedObject)) {
							return 1;
						}
					} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
						e.printStackTrace();
						return -1;
					}
				}
				return 0;
			});
			if (status == 0) {
				try {
					field.set(entity, generatedObject);
				} catch (IllegalArgumentException | IllegalAccessException e) {
				}
				return generatedObject;
			} else if (status == -1) {
				return null;
			}
		}
		return null;
	}

}
