package repository.data.generator;

import java.lang.reflect.Field;
import java.util.Random;

import collection.sync.SyncHashMap;

@GeneratorKey()
public class LongGenerator extends Generator<Long> {

	private final Random random = new Random();

	public LongGenerator(Field field) {
		super(field);
		field.setAccessible(true);
	}

	@Override
	public Long generate(Object entity) {
		long generated = random.nextLong();
		try {
			field.set(entity, generated);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return generated;
	}

	@Override
	public Long generateUnique(Object entity, SyncHashMap<Object, ?> dataBase) {
		for (int i = 0; i < maxTries; i++) {
			long generatedObject = random.nextLong();
			int status = dataBase.getSyncManager().syncronize(() -> {
				for (Object content : dataBase.values()) {
					try {
						long existingObject = field.getLong(content);
						if (existingObject == generatedObject) {
							return 1;
						}
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
						return -1;
					}
				}
				return 0;
			});
			if (status == 0) {
				try {
					field.set(entity, generatedObject);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
				return generatedObject;
			} else if (status == -1) {
				return null;
			}
		}
		return null;
	}

}
