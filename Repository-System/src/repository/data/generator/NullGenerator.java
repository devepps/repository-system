package repository.data.generator;

import java.lang.reflect.Field;

import collection.sync.SyncHashMap;

public class NullGenerator extends Generator<Object> {

	public NullGenerator(Field field) {
		super(field);
	}

	@Override
	public Object generate(Object content) {
		return null;
	}

	@Override
	public Object generateUnique(Object content, SyncHashMap<Object, ?> dataBase) {
		return null;
	}

}
