package repository.data.generator;

import java.lang.reflect.Field;
import java.util.UUID;

import collection.sync.SyncHashMap;

@GeneratorKey()
public class UUIDGenerator extends Generator<UUID> {

	public UUIDGenerator(Field field) {
		super(field);
		field.setAccessible(true);
	}

	@Override
	public UUID generate(Object entity) {
		UUID generated = UUID.randomUUID();
		try {
			field.set(entity, generated);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return generated;
	}

	@Override
	public UUID generateUnique(Object entity, SyncHashMap<Object, ?> dataBase) {
		UUID generated = UUID.randomUUID();
		try {
			field.set(entity, generated);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return generated;
	}

}
