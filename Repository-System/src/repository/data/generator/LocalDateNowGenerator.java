package repository.data.generator;

import java.lang.reflect.Field;
import java.time.LocalDate;

import collection.sync.SyncHashMap;

@GeneratorKey()
public class LocalDateNowGenerator extends Generator<LocalDate> {
	
	public LocalDateNowGenerator(Field field) {
		super(field);
		this.field.setAccessible(true);
	}

	@Override
	public LocalDate generate(Object entity) {
		LocalDate generated = LocalDate.now();
		try {
			field.set(entity, generated);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return generated;
	}

	@Override
	public LocalDate generateUnique(Object entity, SyncHashMap<Object, ?> dataBase) {
		for (int i = 0; i < maxTries; i++) {
			LocalDate generatedObject = LocalDate.now().plusDays(i);
			int status = dataBase.getSyncManager().syncronize(() -> {
				for (Object content : dataBase.values()) {
					try {
						LocalDate existingObject = (LocalDate) field.get(content);
						if (existingObject.isEqual(generatedObject)) {
							return 1;
						}
					} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
						e.printStackTrace();
						return -1;
					}
				}
				return 0;
			});
			if (status == 0) {
				try {
					field.set(entity, generatedObject);
				} catch (IllegalArgumentException | IllegalAccessException e) {
				}
				return generatedObject;
			} else if (status == -1) {
				return null;
			}
		}
		return null;
	}

}
