package repository.data.generator;

import java.lang.reflect.Field;

import collection.sync.SyncHashMap;
import config.ConfigEnvironment;

public abstract class Generator<ContentType> {

	protected int maxTries = 100;
	protected Field field;

	public Generator(Field field) {
		GeneratorKey key = this.getClass().getDeclaredAnnotation(GeneratorKey.class);
		if (key != null && key.maxTriesKey() != null && !key.maxTriesKey().equals("")) {
			String property = ConfigEnvironment.getProperty(key.maxTriesKey()).trim();
			try {
				this.maxTries = Integer.parseInt(property);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		} else {
			maxTries = 100;
		}
		
		this.field = field;
		this.field.setAccessible(true);
	}

	public abstract ContentType generate(Object content);

	public abstract ContentType generateUnique(Object content, SyncHashMap<Object, ?> dataBase);

}
