package repository.data.generator;

import java.lang.reflect.Field;
import java.time.LocalTime;

import collection.sync.SyncHashMap;

@GeneratorKey()
public class LocalTimeNowGenerator extends Generator<LocalTime> {
	
	public LocalTimeNowGenerator(Field field) {
		super(field);
		this.field.setAccessible(true);
	}

	@Override
	public LocalTime generate(Object entity) {
		LocalTime generated = LocalTime.now();
		try {
			field.set(entity, generated);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return generated;
	}

	@Override
	public LocalTime generateUnique(Object entity, SyncHashMap<Object, ?> dataBase) {
		for (int i = 0; i < maxTries; i++) {
			LocalTime generatedObject = LocalTime.now().plusHours(i);
			int status = dataBase.getSyncManager().syncronize(() -> {
				for (Object content : dataBase.values()) {
					try {
						LocalTime existingObject = (LocalTime) field.get(content);
						if (existingObject.compareTo(generatedObject) == 0) {
							return 1;
						}
					} catch (IllegalArgumentException | IllegalAccessException | NullPointerException e) {
						e.printStackTrace();
						return -1;
					}
				}
				return 0;
			});
			if (status == 0) {
				try {
					field.set(entity, generatedObject);
				} catch (IllegalArgumentException | IllegalAccessException e) {
				}
				return generatedObject;
			} else if (status == -1) {
				return null;
			}
		}
		return null;
	}

}
