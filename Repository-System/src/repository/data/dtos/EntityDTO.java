package repository.data.dtos;

import java.util.ArrayList;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.MultiDataReaderType;
import baseSC.data.types.SingleDataReaderType;
import baseSC.data.types.SpecialDataReaderType;
import collection.Utills;

@PackagableEntity(configKey = "Connection.DTO-Ids.Repository.internel.EntityDTO")
public class EntityDTO implements DTO {

	public EntityDTO() {
	}

	public EntityDTO(int requestType, int responseType, String repoName) {
		super();
		this.requestType = requestType;
		this.responseType = responseType;
		this.repoName = repoName;
	}

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_0)
	private int requestType;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_0)
	private int responseType;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String repoName;

	@PackagableContent(dataTypeKey = MultiDataReaderType.KEY_ARRAY_LIST, contentTypeKey = SpecialDataReaderType.KEY_SUBENTITY, defaultValueId = DefaultValue.ID_EMPTY_ARRAYLIST)
	private ArrayList<DTO> entities = new ArrayList<>();

	public int getRequestType() {
		return requestType;
	}

	public int getResponseType() {
		return responseType;
	}

	public String getRepoName() {
		return repoName;
	}

	public ArrayList<DTO> getEntities() {
		return entities;
	}

	@Override
	public DTO copy() {
		EntityDTO entityDTO = new EntityDTO(requestType, responseType, repoName);
		entityDTO.entities = Utills.cloneArrayList(entities);
		return entityDTO;
	}

	@Override
	public String toString() {
		return "EntityDTO [requestType=" + requestType + ", responseType=" + responseType + ", repoName=" + repoName
				+ ", entities=" + entities + "]";
	}
}
