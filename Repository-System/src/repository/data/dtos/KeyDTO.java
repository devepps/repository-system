package repository.data.dtos;

import java.io.Serializable;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;

@PackagableEntity(configKey = "Connection.DTO-Ids.Repository.internel.KeyDTO")
public class KeyDTO implements DTO{

	public KeyDTO() {
		super();
	}

	public KeyDTO(int requestType, int responseType, String repoName, Serializable requestContent, int contentId) {
		super();
		this.requestType = requestType;
		this.responseType = responseType;
		this.repoName = repoName;
		this.requestContent = requestContent;
		this.contentId = contentId;
	}

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_0)
	private int requestType;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_0)
	private int responseType;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1, defaultValueId = DefaultValue.ID_STRING_EMPTY)
	private String repoName;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_SERIALIZABLE)
	private Serializable requestContent;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_MINUS_1)
	private int contentId;

	public int getRequestType() {
		return requestType;
	}

	public int getResponseType() {
		return responseType;
	}

	public String getRepoName() {
		return repoName;
	}

	public Serializable getRequestContent() {
		return requestContent;
	}

	public int getContentId() {
		return contentId;
	}

	@Override
	public String toString() {
		return "KeyDTO [requestType=" + requestType + ", responseType=" + responseType + ", repoName=" + repoName
				+ ", requestContent=" + requestContent + ", contentId=" + contentId + "]";
	}

	@Override
	public DTO copy() {
		return new KeyDTO(requestType, responseType, repoName, requestContent, contentId);
	}
}
