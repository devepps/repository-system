package repository.data.entity;

public enum BaseNumberType {

	INTEGER(Integer.class), DOUBLE(Double.class), FLOAT(Float.class), LONG(Long.class);

	private Class<?> type;

	private BaseNumberType(Class<?> type) {
		this.type = type;
	}

	public static BaseNumberType getBaseType(Class<?> targetType) {
		for (BaseNumberType type : values()) {
			if (type.type.isAssignableFrom(targetType)) {
				return type;
			}
		}
		return null;
	}

	public Object switchType(Object key) {
		if (key == null) {
			return null;
		} else if (key.getClass().isPrimitive()) {
			switch (this) {
				case DOUBLE:
					return (double) key;
				case FLOAT:
					return (float) key;
				case INTEGER:
					return (int) key;
				case LONG:
					return (Double) key;
			}
		} else {
			String value = key.toString();
			switch (this) {
				case DOUBLE:
					return Double.parseDouble(value);
				case FLOAT:
					return Float.parseFloat(value);
				case INTEGER:
					return Integer.parseInt(value);
				case LONG:
					return Long.parseLong(value);
			}
		}		
		return key;
	}

//	private Object switchType(Object obj, BaseNumberType originType) {
//		if (obj == null)
//			return null;
//		if (obj.getClass().isPrimitive()) {
//			return switchTypeToPrimitive(obj, originType);
//		} else {
//			Object value = switchTypeToPrimitive(obj, originType);
//			switch (this) {
//				case DOUBLE:
//					return Double.
//				case FLOAT:
//					break;
//				case INTEGER:
//					break;
//				case LONG:
//					break;
//			}
//		}
//		return null;
//	}
//
//	private Object switchTypeToPrimitive(Object obj, BaseNumberType originType) {
//		switch (this) {
//			case DOUBLE:
//				switch (originType) {
//					case DOUBLE:
//						return ((Double)obj).doubleValue();
//					case FLOAT:
//						return ((Float)obj).doubleValue();
//					case INTEGER:
//						return ((Integer)obj).doubleValue();
//					case LONG:
//						return ((Long)obj).doubleValue();
//				}
//				return null;
//			case FLOAT:
//				switch (originType) {
//					case DOUBLE:
//						return ((Double)obj).floatValue();
//					case FLOAT:
//						return ((Float)obj).floatValue();
//					case INTEGER:
//						return ((Integer)obj).floatValue();
//					case LONG:
//						return ((Long)obj).floatValue();
//				}
//				return null;
//			case INTEGER:
//				switch (originType) {
//					case DOUBLE:
//						return ((Double)obj).intValue();
//					case FLOAT:
//						return ((Float)obj).intValue();
//					case INTEGER:
//						return ((Integer)obj).intValue();
//					case LONG:
//						return ((Long)obj).intValue();
//				}
//				return null;
//			case LONG:
//				switch (originType) {
//					case DOUBLE:
//						return ((Double)obj).longValue();
//					case FLOAT:
//						return ((Float)obj).longValue();
//					case INTEGER:
//						return ((Integer)obj).longValue();
//					case LONG:
//						return ((Long)obj).longValue();
//				}
//				return null;
//		}
//		return null;
//	}
}
