package repository.data.entity;

public enum EntityContentType {

	UNIQUE(),
	GENERATED(),
	UNIQUE_GENERATED(),
	STANDART();

}
