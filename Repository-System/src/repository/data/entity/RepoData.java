package repository.data.entity;

import java.util.Arrays;

public class RepoData {

	private String name;
	private EntityField key;
	private EntityField[] data;
	private Class<?> repoEntity;

	public RepoData(String name, EntityField key, EntityField[] data, Class<?> repoEntity) {
		super();
		this.name = name;
		this.key = key;
		this.data = data;
		this.repoEntity = repoEntity;
	}

	public String getName() {
		return name;
	}

	public EntityField getKey() {
		return key;
	}

	public EntityField[] getData() {
		return data;
	}

	public Class<?> getRepoEntity() {
		return repoEntity;
	}

	@Override
	public String toString() {
		return "RepoData [name=" + name + ", key=" + key + ", data=" + Arrays.toString(data) + ", repoEntity="
				+ repoEntity + "]";
	}

	public int getContentId(String fieldNameLowerCase) {
		if (key.getField().getName().toLowerCase().equals(fieldNameLowerCase)) {
			return -1;
		}
		for (int i = 0; i < data.length; i++) {
			if (data[i].getField().getName().toLowerCase().equals(fieldNameLowerCase)) {
				return i;
			}
		}
		return -2;
	}

	public Object transform(Object key, int keyId) {
		if (key != null) {
			EntityField field = null;
			if (keyId == -1) {
				field = this.key;
			} else {
				field = this.data[keyId];
			}
			BaseNumberType targetType = BaseNumberType.getBaseType(field.getField().getType());
			BaseNumberType originType = BaseNumberType.getBaseType(key.getClass());
			if (targetType != null && targetType != originType && originType != null) {
				return targetType.switchType(key);
			}
			return key;
		}
		return null;
	}
}
