package repository.data.entity;

import java.lang.reflect.Field;

import baseSC.data.packageType.PackagableContent;
import baseSC.data.types.DefaultValue;
import repository.data.generator.Generator;

public class EntityField {

	private Field field;
	private Generator<?> generator;
	private EntityContentType contentType;

	public EntityField(Field field, Generator<?> generator, EntityContentType contentType) {
		super();
		this.field = field;
		this.generator = generator;
		this.contentType = contentType;
		
		this.field.setAccessible(true);
	}

	public Field getField() {
		return field;
	}

	public Generator<?> getGenerator() {
		return generator;
	}

	public EntityContentType getContentType() {
		return contentType;
	}

	@Override
	public String toString() {
		return "EntityField [field=" + field + ", generator=" + generator + ", contentType=" + contentType + "]";
	}

	public boolean isEmpty(Object master) {
		PackagableContent content = field.getDeclaredAnnotation(PackagableContent.class);
		Object nullValue = DefaultValue.fromId(content.defaultValueId()).getDefaultValue();
		Object currentValue = null;
		try {
			currentValue = field.get(master);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return currentValue == null ? nullValue == null : currentValue.equals(nullValue);
	}

}
