package repository.data.entity;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import repository.data.generator.Generator;
import repository.data.generator.NullGenerator;

@Retention(RUNTIME)
@Target(FIELD)
public @interface DatabaseEntry {
	
	public EntryType type();
	public EntityContentType contentType() default EntityContentType.STANDART;
	public Class<? extends Generator<?>> generator() default NullGenerator.class;
}
