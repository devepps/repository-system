package repository.data;

public enum RequestType {

	INAVLID(0),
	GET(1),
	SET(2),
	OVERWRITE(3),
	ADD(4),
	REMOVE(5),
	CLEAR(6);

	private int id;

	private RequestType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static RequestType fromId(int typeId) {
		for (RequestType type : values()) {
			if(type.id == typeId) {
				return type;
			}
		}
		return INAVLID;
	}
}
