package repository.data.exceptions;

import data.exceptions.CustomException;

public class InvalidKeyException extends CustomException {

	private static final long serialVersionUID = 1L;

	public InvalidKeyException(Object key, Object entity, Throwable e) {
		super(e, "The key " + key + " of the entity " + entity + " is invalid.");
	}

	public InvalidKeyException(Object key, Object entity) {
		super(null, "The key " + key + " of the entity " + entity + " is invalid.");
	}

}
