package repository.data.exceptions;

import baseSC.data.DTO;
import data.exceptions.CustomException;

public class InvalidRepositoryException extends CustomException {

	private static final long serialVersionUID = 1L;

	public InvalidRepositoryException(Class<? extends DTO> entityClass, Throwable e) {
		super(e, "An error occured while creating new Repo: " + entityClass);
	}

	public InvalidRepositoryException(Class<? extends DTO> entityClass) {
		super(null, "An error occured while creating new Repo: " + entityClass);
	}

}
