package repository.data.exceptions;

import data.exceptions.CustomException;

public class InvalidEntityException extends CustomException {

	private static final long serialVersionUID = 1L;

	public InvalidEntityException(Object entity, Throwable e) {
		super(e, "The entity " + entity + " is invalid or contains invalid data.");
	}

	public InvalidEntityException(Object entity) {
		super(null, "The entity " + entity + " is invalid or contains invalid data.");
	}

}
