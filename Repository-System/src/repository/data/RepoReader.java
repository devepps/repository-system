package repository.data;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

import baseSC.data.DTO;
import baseSC.data.packageType.PackageManager;
import collection.sync.SyncArrayList;
import repository.data.dtos.EntityDTO;
import repository.data.dtos.KeyDTO;
import repository.data.entity.DatabaseEntity;
import repository.data.entity.DatabaseEntry;
import repository.data.entity.EntityContentType;
import repository.data.entity.EntityField;
import repository.data.entity.EntryType;
import repository.data.entity.RepoData;
import repository.data.exceptions.InvalidRepositoryException;
import repository.logger.RepositoryLogger;

public class RepoReader {

	private final SyncArrayList<String> repoNames = new SyncArrayList<>();

	private PackageManager packageManager;

	public RepoReader(PackageManager packageManager) {
		super();
		this.packageManager = packageManager;
		this.packageManager.createPackageType(EntityDTO.class);
		this.packageManager.createPackageType(KeyDTO.class);
	}

	public <EntityType extends DTO> RepoData readRepository(Class<EntityType> entityClass) {
		DatabaseEntity entityInfo = entityClass.getDeclaredAnnotation(DatabaseEntity.class);
		if (entityInfo == null || entityInfo.name() == null || entityInfo.name().trim().equals("")) {
			throw new InvalidRepositoryException(entityClass);
		}
		boolean nameIsKnown = repoNames.contains(entityInfo.name());
		if (nameIsKnown) {
			throw new InvalidRepositoryException(entityClass);
		}		
		repoNames.add(entityInfo.name());

		EntityField key = null;
		ArrayList<EntityField> data = new ArrayList<>();
		Field[] declaredFields = getFields(entityClass);
		System.out.println(" === " + entityClass.getName() + " === ");		
		for (Field field : declaredFields) {
			System.out.println(field.getName());
			DatabaseEntry databaseEntry = field.getDeclaredAnnotation(DatabaseEntry.class);
			if (databaseEntry != null) {
				if (databaseEntry.contentType() == null || databaseEntry.type() == null) {
					throw new InvalidRepositoryException(entityClass);
				}
				if (databaseEntry.type() == EntryType.KEY) {
					if (key != null || databaseEntry.generator() == null) {
						throw new InvalidRepositoryException(entityClass);
					}
					try {
						key = new EntityField(field,
								databaseEntry.generator().getDeclaredConstructor(Field.class).newInstance(field),
								databaseEntry.contentType());
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | NoSuchMethodException | SecurityException e) {
						e.printStackTrace();
						throw new InvalidRepositoryException(entityClass);
					}
				} else {
					if (databaseEntry.generator() == null && (databaseEntry.contentType() == EntityContentType.GENERATED
							|| databaseEntry.contentType() == EntityContentType.UNIQUE_GENERATED)) {
						throw new InvalidRepositoryException(entityClass);
					}
					try {
						data.add(new EntityField(field,
								databaseEntry.generator().getDeclaredConstructor(Field.class).newInstance(field),
								databaseEntry.contentType()));
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | NoSuchMethodException | SecurityException e) {
						e.printStackTrace();
						throw new InvalidRepositoryException(entityClass);
					}
				}
			}
		}

		if (key == null) {
			throw new InvalidRepositoryException(entityClass);
		}
		packageManager.createPackageType(entityClass);
		RepoData repoData = new RepoData(entityInfo.name(), key, data.toArray(new EntityField[data.size()]), entityClass);
		RepositoryLogger.debug("RepoData created for entity class " + entityClass + " : " + repoData, entityInfo.name());
		return repoData;
	}

	private <EntityType> Field[] getFields(Class<EntityType> entityClass) {
		ArrayList<Field> fields = new ArrayList<>();
		Class<?> currentClass = entityClass;
		while (currentClass.getSuperclass() != null && !Object.class.equals(currentClass)) {
			fields.addAll(Arrays.asList(currentClass.getDeclaredFields()));
			currentClass = currentClass.getSuperclass();
		}
		return fields.toArray(new Field[fields.size()]);
	}

}
