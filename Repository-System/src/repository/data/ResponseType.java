package repository.data;

public enum ResponseType {

	DATA_CREATED(21001),
	ENTITY_REPLACED(21002),
	REPOSITORY_UNKNOWN(33001),
	ENTITY_ALREADY_EXISTED(33002),
	ENTITY_NOT_FOUND(33003),
	;

	private int id;

	private ResponseType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
