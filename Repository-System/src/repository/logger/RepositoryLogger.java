package repository.logger;

import config.ConfigEnvironment;
import logger.main.Logger;
import logger.main.prefixs.TimePrefix;

public class RepositoryLogger extends Logger{

	private static final boolean debug = Boolean.parseBoolean(ConfigEnvironment.getProperty("Status.Repository.Debug"));

	private static final RepositoryLogger logger = new RepositoryLogger();
	
	private RepositoryLogger() {
		super("Repository", new TimePrefix("dd.MM.yy"), new TimePrefix("hh:mm:ss"));
	}
	
	public static void debug(String text) {
		if(!debug) return;
		logger.println(text);
	}
	
	public static void debug(String text, String repoName) {
		if(!debug) return;
		logger.println("[" + repoName + "] " + text);
	}

	public static void debug(String text, Throwable e, String repoName) {
		if(!debug) return;
		logger.println("[" + repoName + "] " + text + " An error occured: " + e.getMessage());		
		
	}

}
