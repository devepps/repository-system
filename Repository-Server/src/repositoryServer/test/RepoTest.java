package repositoryServer.test;

import java.io.IOException;

import collection.sync.SyncManager;
import collection.tick.TickManager;
import repositoryServer.data.Repository;
import repositoryServer.server.RepoServer;

public class RepoTest {

	public static void main(String[] args) {
		new RepoTest();
	}

	public RepoTest() {
		RepoServer server = new RepoServer("Repository.Test.Server.Port", "Repository.Test.Server.KeyStorePath", "Repository.Test.Server.KeyStorePassword");

		Repository repo = server.registerRepo(SimpleRepo.class);

		new TickManager(() -> server.tick(), 1);

		new Thread(new Runnable() {
			SyncManager syncManager = new SyncManager();

			@Override
			public void run() {
				while (true) {
					try {
						repo.save();
					} catch (IOException e) {
						e.printStackTrace();
					}
					syncManager.asyncronizedWait(10000);
				}
			}
		}).start();
	}
}
