package repositoryServer.test;

import java.io.Serializable;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;
import repository.data.entity.DatabaseEntity;
import repository.data.entity.DatabaseEntry;
import repository.data.entity.EntityContentType;
import repository.data.entity.EntryType;
import repository.data.generator.IntegerGenerator;
import repository.data.generator.LongGenerator;

@PackagableEntity(configKey = "Connection.DTO-Ids.Repository.test.SimpleRepo")
@DatabaseEntity(name = "SimpleRepo")
public class SimpleRepo implements DTO, Serializable{

	private static final long serialVersionUID = -5079626147105074068L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_0)
	@DatabaseEntry(type = EntryType.KEY, contentType = EntityContentType.UNIQUE_GENERATED, generator = IntegerGenerator.class)
	private int id;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1)
	@DatabaseEntry(type = EntryType.DATA)
	private String text;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_LONG, defaultValueId = DefaultValue.ID_LONG_0)
	@DatabaseEntry(type = EntryType.DATA, contentType = EntityContentType.GENERATED, generator = LongGenerator.class)
	private long generated;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_LONG, defaultValueId = DefaultValue.ID_LONG_0)
	@DatabaseEntry(type = EntryType.DATA, contentType = EntityContentType.UNIQUE_GENERATED, generator = LongGenerator.class)
	private long uniqueGenerated;

	public SimpleRepo(int id, String text, long generated, long uniqueGenerated) {
		super();
		this.id = id;
		this.text = text;
		this.generated = generated;
		this.uniqueGenerated = uniqueGenerated;
	}

	public SimpleRepo() {
		super();
	}

	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public long getGenerated() {
		return generated;
	}

	public long getUniqueGenerated() {
		return uniqueGenerated;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setGenerated(long generated) {
		this.generated = generated;
	}

	public void setUniqueGenerated(long uniqueGenerated) {
		this.uniqueGenerated = uniqueGenerated;
	}

	@Override
	public String toString() {
		return "SimpleRepo [id=" + id + ", text=" + text + ", generated=" + generated + ", uniqueGenerated="
				+ uniqueGenerated + "]";
	}

	@Override
	public DTO copy() {
		return new SimpleRepo(id, text, generated, uniqueGenerated);
	}
}
