package repositoryServer.data.exceptions;

import data.exceptions.CustomException;
import repositoryServer.data.Repository;

public class RepositoryException extends CustomException {

	private static final long serialVersionUID = 1L;

	public RepositoryException(Object entity, Repository repository, Throwable e) {
		super(e, "An unkown error eccured while handling " + entity + " in repository " + repository + ".");
	}

	public RepositoryException(Object entity, Repository repository) {
		super(null, "An unkown error eccured while handling " + entity + " in repository " + repository + ".");
	}

}
