package repositoryServer.data.exceptions;

import baseSC.data.DTO;
import data.exceptions.CustomException;

public class RequestException extends CustomException {

	private static final long serialVersionUID = 1L;

	public RequestException(DTO request, Throwable e) {
		super(e, "The requested action of request " + request + " is unknown to the server.");
	}

	public RequestException(DTO request) {
		super(null, "The requested action of request " + request + " is unknown to the server.");
	}

}
