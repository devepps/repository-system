package repositoryServer.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Map;

import baseSC.data.DTO;
import collection.sync.SyncHashMap;
import config.ConfigEnvironment;
import repository.data.entity.EntityField;
import repository.data.entity.RepoData;
import repository.data.exceptions.InvalidEntityException;
import repository.data.exceptions.InvalidKeyException;
import repository.logger.RepositoryLogger;
import repositoryServer.data.exceptions.RepositoryException;

public class Repository {

	private final String basePath = ConfigEnvironment.getProperty("Repository.Files.basePath");

	private final SyncHashMap<Object, DTO> dataBase = new SyncHashMap<>();
	private RepoData repoData;
	private File file;

	public Repository(RepoData repoData) {
		super();
		this.repoData = repoData;

		repoData.getKey().getField().setAccessible(true);
		for (EntityField field : repoData.getData()) {
			field.getField().setAccessible(true);
		}

		this.file = new File(basePath + "/" + repoData.getName() + ".repo");
		if(file.exists()) {
			try {
				load();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public DTO set(DTO content) {
		if (content == null) {
			throw new InvalidEntityException(content);
		}
		try {
			Object key = this.repoData.getKey().isEmpty(content) ? this.repoData.getKey().getGenerator().generate(content)
					: this.repoData.getKey().getField().get(content);
			if (key == null) {
				throw new InvalidKeyException(key, content);
			}
			getCompleteContent(content);
			if(dataBase.put(key, content) != null ) {
				return content;
			}
			return null;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RepositoryException(content, this);
		}
	}

	public DTO overwrite(DTO content) {
		if (content == null) {
			throw new InvalidEntityException(content);
		}
		try {
			Object key = this.repoData.getKey().getField().get(content);
			if (this.repoData.getKey().isEmpty(content) || key == null) {
				throw new InvalidKeyException(key, content);
			}
			getCompleteContent(content);
			if (dataBase.containsKey(key)) {
				dataBase.put(key, content);
				return content;
			}
			return null;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RepositoryException(content, this);
		}
	}

	public DTO add(DTO content) {
		if (content == null) {
			throw new InvalidEntityException(content);
		}
		try {
			Object key = this.repoData.getKey().isEmpty(content) ? this.repoData.getKey().getGenerator().generate(content)
					: this.repoData.getKey().getField().get(content);
			if (key == null) {
				throw new InvalidKeyException(key, content);
			}
			getCompleteContent(content);
			if (!dataBase.containsKey(key)) {
				dataBase.put(key, content);
				return content;
			}
			return null;
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RepositoryException(content, this);
		}
	}

	private void getCompleteContent(DTO content) throws IllegalArgumentException, IllegalAccessException  {
		for (EntityField field : repoData.getData()) {
			switch (field.getContentType()) {
				case GENERATED:
					if (field.isEmpty(content)) {
						field.getGenerator().generate(content);
					}
					break;
				case UNIQUE:
					checkUnique(field, content);
					break;
				case UNIQUE_GENERATED:
					if (field.isEmpty(content)) {
						field.getGenerator().generateUnique(content, dataBase);
					} else {
						boolean isUnique = true;
						try {
							checkUnique(field, content);
						} catch (Throwable e) {
							isUnique = false;
						}
						if (!isUnique) {
							field.getGenerator().generateUnique(content, dataBase);
						}
					}
					break;
				default:
					break;
			}
		}
	}

	private void checkUnique(EntityField field, DTO content)
			throws IllegalArgumentException, IllegalAccessException {
		Object uniqueFieldContent = field.getField().get(content);
		if (uniqueFieldContent == null) {
			return;
		}
		boolean isUnique = dataBase.getSyncManager().syncronize(() -> {
			for (DTO entity : dataBase.values()) {
				try {
					if (uniqueFieldContent.equals(field.getField().get(entity))) {
						return false;
					}
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			return true;
		});
		if (!isUnique) {
			throw new InvalidEntityException(content);
		}
	}

	public ArrayList<DTO> get(Object keypara, int keyId) {
		Object key = repoData.transform(keypara, keyId);
		if (keyId == -1) {
			if (key == null) {
				throw new InvalidKeyException(key, null);
			}
			ArrayList<DTO> found = new ArrayList<>();
			DTO data = dataBase.get(key);
			if (data != null) {
				found.add(data);
			}
			return found;
		} else {
			if (key == null) {
				throw new InvalidKeyException(key, null);
			}
			ArrayList<DTO> found = new ArrayList<>();
			dataBase.getSyncManager().syncronize(() -> {
				for (DTO data : dataBase.values()) {
					try {
						if (key.equals(repoData.getData()[keyId].getField().get(data))) {
							found.add(data);
						}
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			});
			return found;
		}
	}

	public void remove(Object keypara, int keyId) {
		Object key = repoData.transform(keypara, keyId);
		if (keyId == -1) {
			if (key == null) {
				throw new InvalidKeyException(key, null);
			}
			dataBase.remove(key);
		} else {
			if (key == null) {
				throw new InvalidKeyException(key, null);
			}
			dataBase.getSyncManager().syncronize(() -> {
				for (DTO data : dataBase.values()) {
					try {
						if (key.equals(repoData.getData()[keyId].getField().get(data))) {
							dataBase.remove(key);
						}
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			});
		}
	}

	public void clear() {
		dataBase.clear();
	}

	public void save() throws IOException {
		if (!file.exists()) {
			int lastIndex1 = this.file.getAbsolutePath().lastIndexOf('/');
			int lastIndex2 = this.file.getAbsolutePath().lastIndexOf('\\');
			new File(this.file.getAbsolutePath().substring(0, lastIndex1 > lastIndex2 ? lastIndex1 : lastIndex2)).mkdirs();
			file.createNewFile();
		}
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		fileOutputStream.getChannel().position(0);

		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(fileOutputStream);
			out.writeObject(dataBase.clone());
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (out != null) {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		fileOutputStream.flush();
		fileOutputStream.close();
	}

	@SuppressWarnings("unchecked")
	public void load() throws IOException {
		FileInputStream fileInputStream = new FileInputStream(file);
		fileInputStream.getChannel().position(0);

		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(fileInputStream);
			this.dataBase.putAll((Map<? extends Object, ? extends DTO>) in.readObject());
			RepositoryLogger.debug("Repository loaded : " + dataBase, repoData.getName());
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (in != null) {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		fileInputStream.close();
	}
	
	public String toString() {
		String string = this.repoData.getKey().getField().getName() + ";";
		for (EntityField data : this.repoData.getData()) {
			string = string + data.getField().getName() + ";";
		}
		return string + "\n" + this.dataBase.getSyncManager().syncronize(() -> {
			String str = "";
			for (Object key : this.dataBase.getUnsyncronized().keySet()) {
				str += key + ";";
				Object value = this.dataBase.getUnsyncronized().get(key);
				for (EntityField data : this.repoData.getData()) {
					try {
						Object fieldValue = data.getField().get(value);
						str += fieldValue + "";
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
					str += ";";
				}
				str += "\n";
			}
			return str;
		});
	}

}