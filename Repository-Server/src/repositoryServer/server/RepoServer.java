package repositoryServer.server;

import java.io.Serializable;
import java.util.ArrayList;

import baseSC.data.DTO;
import baseSC.data.subscriber.SubscribedStatus;
import baseSC.server.ServerManager;
import collection.sync.SyncHashMap;
import config.ConfigEnvironment;
import repository.data.RepoReader;
import repository.data.RequestType;
import repository.data.ResponseType;
import repository.data.dtos.EntityDTO;
import repository.data.dtos.KeyDTO;
import repository.data.entity.RepoData;
import repository.logger.RepositoryLogger;
import repositoryServer.data.Repository;

public class RepoServer {

	private final SyncHashMap<String, Repository> repos = new SyncHashMap<>();

	private ServerManager serverManager;
	private RepoReader repoReader;

	public RepoServer(String portPath, String sslKeyStorePath, String sslKeyStorePasswordPath) {
		serverManager = new ServerManager(Integer.parseInt(ConfigEnvironment.getProperty(portPath)),
				sslKeyStorePath, sslKeyStorePasswordPath);
		serverManager.openConnection();
		serverManager.getEventManager().enableParallelMode();

		new RepoServerEM(this, serverManager);

		this.repoReader = new RepoReader(serverManager.getDataPackageManager());
	}

	public Repository getRepository(String repoName) {
		return repos.get(repoName);
	}

	public void send(long clientId, EntityDTO entityDTO) {
		serverManager.sendMessage(clientId, entityDTO);
	}

	public void tick() {
		serverManager.getEventManager().tick();
	}

	public Repository registerRepo(Class<? extends DTO> entityClass) {
		RepoData repoData = this.repoReader.readRepository(entityClass);
		Repository repo = new Repository(repoData);
		repos.put(repoData.getName(), repo);
		return repo;
	}

	public <EntityClass extends DTO> EntityDTO handleAddRequest(Repository repo, EntityDTO request) {
		ArrayList<DTO> entities = request.getEntities();
		if (entities != null && entities.size() == 1) {
			DTO responseEntity = repo.add(entities.get(0));
			if (responseEntity == null) {
				return new EntityDTO(RequestType.INAVLID.getId(), ResponseType.ENTITY_ALREADY_EXISTED.getId(), null);
			}
			EntityDTO response = new EntityDTO(RequestType.INAVLID.getId(), ResponseType.DATA_CREATED.getId(), null);
			response.getEntities().add(responseEntity);
			return response;
		}
		return new EntityDTO(RequestType.INAVLID.getId(), SubscribedStatus.BAD_REQUEST.getId(), null);
	}

	public EntityDTO handleGetRequest(Repository repo, KeyDTO request) {
		Object key = request.getRequestContent();
		if (key != null) {
			RepositoryLogger.debug("GET(" + key + "|" + key.getClass() + "|" + ((Serializable) key) + ")");
			ArrayList<DTO> entities = repo.get(key, request.getContentId());
			if (entities != null) {
				EntityDTO response = new EntityDTO(RequestType.INAVLID.getId(), ResponseType.DATA_CREATED.getId(),
						null);
				response.getEntities().addAll(entities);
				return response;
			}
		}
		return new EntityDTO(RequestType.INAVLID.getId(), SubscribedStatus.BAD_REQUEST.getId(), null);
	}

	public EntityDTO handleOverwriteRequest(Repository repo, EntityDTO request) {
		ArrayList<DTO> entities = request.getEntities();
		if (entities != null && entities.size() == 1) {
			DTO responseEntity = repo.overwrite(entities.get(0));
			if (responseEntity == null) {
				return new EntityDTO(RequestType.INAVLID.getId(), ResponseType.ENTITY_NOT_FOUND.getId(), null);
			}
			EntityDTO response = new EntityDTO(RequestType.INAVLID.getId(), ResponseType.DATA_CREATED.getId(), null);
			response.getEntities().add(responseEntity);
			return response;
		}
		return new EntityDTO(RequestType.INAVLID.getId(), SubscribedStatus.BAD_REQUEST.getId(), null);
	}

	public EntityDTO handleDeleteRequest(Repository repo, KeyDTO request) {
		if (request.getRequestContent() != null) {
			repo.remove(request.getRequestContent(), request.getContentId());
		}
		return new EntityDTO(RequestType.INAVLID.getId(), SubscribedStatus.BAD_REQUEST.getId(), null);
	}

	public EntityDTO handleSetRequest(Repository repo, EntityDTO request) {
		ArrayList<DTO> entities = request.getEntities();
		if (entities != null && entities.size() == 1) {
			DTO responseEntity = repo.set(entities.get(0));
			if (responseEntity == null) {
				return new EntityDTO(RequestType.INAVLID.getId(), SubscribedStatus.BAD_REQUEST.getId(), null);
			}
			EntityDTO response = new EntityDTO(RequestType.INAVLID.getId(), ResponseType.DATA_CREATED.getId(), null);
			response.getEntities().add(responseEntity);
			return response;
		}
		return new EntityDTO(RequestType.INAVLID.getId(), SubscribedStatus.BAD_REQUEST.getId(), null);
	}
}
