package repositoryServer.server;

import baseSC.data.DTO;
import baseSC.data.events.server.NewClientConnectionEvent;
import baseSC.data.events.server.NewClientConnectionEventListener;
import baseSC.data.events.server.ServerLostConnectionToClientEvent;
import baseSC.data.events.server.ServerLostConnectionToClientEventListener;
import baseSC.data.events.server.SubscribedMessageEvent;
import baseSC.data.events.server.ToServerMessageEvent;
import baseSC.data.events.server.ToServerMessageEventListener;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.subscriber.SubscribedStatus;
import baseSC.server.ServerManager;
import repository.data.RequestType;
import repository.data.ResponseType;
import repository.data.dtos.EntityDTO;
import repository.data.dtos.KeyDTO;
import repository.logger.RepositoryLogger;
import repositoryServer.data.Repository;
import repositoryServer.data.exceptions.RequestException;

public class RepoServerEM implements ToServerMessageEventListener, NewClientConnectionEventListener,
		ServerLostConnectionToClientEventListener {

	private RepoServer repoServer;

	public RepoServerEM(RepoServer repoServer, ServerManager serverManager) {
		this.repoServer = repoServer;

		serverManager.getEventManager().registerNewClientConnectionEventListener(this, 0);
		serverManager.getEventManager().registerServerLostConnectionToClientEventListener(this, 0);
		serverManager.getEventManager().registerServerMessageEventListener(this, 0);
	}

	@Override
	public void newServerClient(NewClientConnectionEvent event) {
		RepositoryLogger.debug("Repository has connected to a new client: " + event.getClientID());
		event.setActive(false);
	}

	@Override
	public void messageFromClient(ToServerMessageEvent event) {
		if (event.getObjectDTO() instanceof EntityDTO || event.getObjectDTO() instanceof KeyDTO) {
			String repoName = (event.getObjectDTO() instanceof EntityDTO)
					? ((EntityDTO) event.getObjectDTO()).getRepoName() 
					: ((KeyDTO) event.getObjectDTO()).getRepoName();
			int requestType = (event.getObjectDTO() instanceof EntityDTO)
					? ((EntityDTO) event.getObjectDTO()).getRequestType()
					: ((KeyDTO) event.getObjectDTO()).getRequestType();

			Repository repo = this.repoServer.getRepository(repoName);
			if (repo == null) {
				event.setActive(false);
				return;
			}

			try {
				EntityDTO answer = handleRequest(repo, (DTO) event.getObjectDTO(), RequestType.fromId(requestType));
				if (answer != null) {
					repoServer.send(event.getClientID(), answer);
					RepositoryLogger.debug("The request " + event.getObjectDTO() + " has been answered with " + answer + ".");
				}
			} catch (Throwable e) {
				RepositoryLogger.debug("An error occured while handling: " + event.getObjectDTO(), e, repoName);
			}
		} else {
			RepositoryLogger.debug("An unknown request has occured: " + event.getObjectDTO());
		}
		event.setActive(false);
	}

	@Override
	public void messageFromClient(SubscribedMessageEvent event) {
		RepositoryLogger.debug("New Message (" + event.getRequest() + ")");
		if (event.getRequest().getContent() instanceof EntityDTO || event.getRequest().getContent() instanceof KeyDTO) {
			DTO requestObj = event.getRequest().getContent();
			String repoName = (requestObj instanceof EntityDTO)
					? ((EntityDTO) requestObj).getRepoName() 
					: ((KeyDTO) requestObj).getRepoName();
			int requestType = (requestObj instanceof EntityDTO)
					? ((EntityDTO) requestObj).getRequestType()
					: ((KeyDTO) requestObj).getRequestType();

			Repository repo = this.repoServer.getRepository(repoName);
			if (repo == null) {
				event.setAnswer(new SubscribedMessageDTO(event.getRequest().getSmHeader(),
						new EntityDTO(RequestType.INAVLID.getId(), ResponseType.REPOSITORY_UNKNOWN.getId(), repoName)));
				event.setActive(false);
				return;
			}

			try {
				EntityDTO answer = handleRequest(repo, requestObj, RequestType.fromId(requestType));
				if (answer != null) {
					event.setAnswer(new SubscribedMessageDTO(event.getRequest().getSmHeader(), answer));
					if (event.getAnswer().getSmHeader().getStatusId() == SubscribedStatus.PENDING.getId()) {
						event.getAnswer().getSmHeader().setStatus(SubscribedStatus.OK.getId());
					}
					RepositoryLogger.debug("The request " + requestObj + " has been answered with " + answer + ".");
				} else {
					event.setAnswer(new SubscribedMessageDTO(event.getRequest().getSmHeader(),
							new EntityDTO(RequestType.INAVLID.getId(), SubscribedStatus.FAILURE.getId(), repoName)));
				}
			} catch (Throwable e) {
				event.setAnswer(new SubscribedMessageDTO(event.getRequest().getSmHeader(),
						new EntityDTO(RequestType.INAVLID.getId(), SubscribedStatus.FAILURE.getId(), repoName)));
				RepositoryLogger.debug("An error occured while handling: " + requestObj, e, repoName);
				e.printStackTrace();
			}
		} else {
			RepositoryLogger.debug("An unknown request has occured: " + event.getRequest());
		}
		event.setActive(false);
	}

	@Override
	public void connectionLost(ServerLostConnectionToClientEvent event) {
		RepositoryLogger.debug("Repository lost connection to client: " + event.getClientID());
		event.setActive(false);
	}

	private EntityDTO handleRequest(Repository repo, DTO request, RequestType requestType) {
		switch (requestType) {
			case ADD:
				if (request instanceof EntityDTO) {
					return repoServer.handleAddRequest(repo, (EntityDTO) request);
				}
				throw new RequestException((KeyDTO) request);
			case CLEAR:
				repo.clear();
				return new EntityDTO(RequestType.INAVLID.getId(), SubscribedStatus.OK.getId(), null);
			case GET:
				if (request instanceof KeyDTO) {
					return repoServer.handleGetRequest(repo, (KeyDTO) request);
				}
				throw new RequestException((KeyDTO) request);
			case INAVLID:
				throw new RequestException(request);
			case OVERWRITE:
				if (request instanceof EntityDTO) {
					return repoServer.handleOverwriteRequest(repo, (EntityDTO) request);					
				}
				throw new RequestException((KeyDTO) request);
			case REMOVE:
				if (request instanceof KeyDTO) {
					return repoServer.handleDeleteRequest(repo, (KeyDTO) request);
				}
				throw new RequestException((KeyDTO) request);
			case SET:
				if (request instanceof EntityDTO) {
					return repoServer.handleSetRequest(repo, (EntityDTO) request);					
				}
				throw new RequestException((KeyDTO) request);
		}
		return null;
	}

}
