package repositoryClient.test;

import collection.tick.TickManager;
import repositoryClient.client.RepoClientInterface;
import repositoryClient.data.Repository;

public class RepoTest {
	
	public static void main(String args[]) {
		new RepoTest();
	}
	
	private RepoClientInterface repoClient;
	private Repository<SimpleRepo> repo;
	
	public RepoTest() {
		repoClient = new RepoClientInterface("Repository.Client.Connection.Test.Ip", "Repository.Client.Connection.Test.Port", "Repository.Client.Connection.Test.keyStorePath");
		repo = repoClient.createRepository(SimpleRepo.class);

		new TickManager(() -> repoClient.tick(), 1);
		
		SimpleRepo entity1R = new SimpleRepo(0, "test-1", 0, 0);
		SimpleRepo entity1A = repo.add(entity1R);
		System.out.println("Server answered to ADD:" + entity1R + " with  " + entity1A);
		
		SimpleRepo entity2A = repo.getByKey(entity1A.getId());
		System.out.println("Server answered to GET:" + entity1R.getId() + " with  " + entity1A);
		
		repo.removeByKey(entity2A.getId());
		
		SimpleRepo entity3A = repo.getByKey(entity2A.getId());
		System.out.println("Server answered to GET:" + entity2A.getId() + " with  " + entity3A);
		
		SimpleRepo entity1B = repo.add(entity1R);
		System.out.println("Server answered to ADD:" + entity1R + " with  " + entity1B);
		SimpleRepo entity1C = repo.add(entity1R);
		System.out.println("Server answered to ADD:" + entity1R + " with  " + entity1C);
		SimpleRepo entity1D = repo.add(entity1B);
		System.out.println("Server answered to ADD:" + entity1R + " with  " + entity1D);
	}

}
