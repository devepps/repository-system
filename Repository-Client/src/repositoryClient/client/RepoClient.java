package repositoryClient.client;

import baseSC.client.ClientManager;
import baseSC.data.DTO;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import baseSC.data.packageType.PackageManager;
import config.ConfigEnvironment;

public class RepoClient {

	private static final long MESSAGE_VALID_DURATION = Long.parseLong(ConfigEnvironment.getProperty("Repository.Messages.ValidDuration").trim());

	private ClientManager clientManager;

	public RepoClient(String ipKeyPath, String portKeyPath, String keyStorePathKeyPath) {
		clientManager = new ClientManager(ConfigEnvironment.getProperty(ipKeyPath),
				Integer.parseInt(ConfigEnvironment.getProperty(portKeyPath)),
				keyStorePathKeyPath);
		clientManager.connectToServer();
	}

	public SubscribedMessageDTO sendSubscribedMessage(DTO message) {
		return this.clientManager.subscribe(message, MESSAGE_VALID_DURATION);
	}

	public void sendMessage(DTO message) {
		this.clientManager.sendMessage(message);
	}

	public PackageManager getPackageManager() {
		return clientManager.getPackageManager();
	}

	public void tick() {
		clientManager.getEventManager().tick();
	}

}
