package repositoryClient.client;

import baseSC.data.DTO;
import collection.sync.SyncHashMap;
import repository.data.RepoReader;
import repository.data.entity.RepoData;
import repositoryClient.data.Repository;

public class RepoClientInterface {
	
	private final SyncHashMap<String, Repository<? extends DTO>> repos = new SyncHashMap<>();

	private RepoClient repoClient;
	private RepoReader repoReader;

	public RepoClientInterface(String ipKeyPath, String portKeyPath, String keyStorePathKeyPath) {
		this.repoClient = new RepoClient(ipKeyPath, portKeyPath, keyStorePathKeyPath);
		this.repoReader = new RepoReader(repoClient.getPackageManager());
	}

	public <EntityClass extends DTO> Repository<EntityClass> createRepository(Class<? extends EntityClass> entityClass) {
		RepoData repoData = repoReader.readRepository(entityClass);
		Repository<EntityClass> repo = new Repository<>(repoClient, repoData);
		repos.put(repoData.getName(), repo);
		return repo;
	}

	@SuppressWarnings("unchecked")
	public <EntityClass extends DTO> Repository<EntityClass> getRepository(String repoName) {
		return (Repository<EntityClass>) repos.get(repoName);
	}

	public void tick() {
		repoClient.tick();
	}
}
