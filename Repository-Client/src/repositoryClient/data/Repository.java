package repositoryClient.data;

import java.io.Serializable;
import java.util.ArrayList;

import baseSC.data.DTO;
import baseSC.data.internelDTO.SubscribedMessageDTO;
import repository.data.RequestType;
import repository.data.dtos.EntityDTO;
import repository.data.dtos.KeyDTO;
import repository.data.entity.RepoData;
import repository.logger.RepositoryLogger;
import repositoryClient.client.RepoClient;

public class Repository<EntityClass extends DTO> {

	private RepoClient repoClient;
	private RepoData repoData;

	public Repository(RepoClient repoClient, RepoData repoData) {
		this.repoClient = repoClient;
		this.repoData = repoData;
	}

	public EntityClass add(EntityClass entity) {
		if (entity == null) {
			RepositoryLogger.debug("Invalid request: ADD (" + entity + ")", repoData.getName());
			return null;
		}

		EntityDTO message = new EntityDTO(RequestType.ADD.getId(), 0, repoData.getName());
		message.getEntities().add(entity);
		SubscribedMessageDTO answer = this.repoClient.sendSubscribedMessage(message);
		RepositoryLogger.debug("The request to add \"" + entity + "\" has been answered with: " + answer,
				repoData.getName());
		return handleAnswer(answer, RequestType.ADD);
	}

	public EntityClass set(EntityClass entity) {
		if (entity == null) {
			RepositoryLogger.debug("Invalid request: SET (" + entity + ")", repoData.getName());
			return null;
		}

		EntityDTO message = new EntityDTO(RequestType.SET.getId(), 0, repoData.getName());
		message.getEntities().add(entity);
		SubscribedMessageDTO answer = this.repoClient.sendSubscribedMessage(message);
		RepositoryLogger.debug("The request to set \"" + entity + "\" has been answered with: " + answer,
				repoData.getName());
		return handleAnswer(answer, RequestType.SET);
	}

	public EntityClass overwrite(EntityClass entity) {
		if (entity == null) {
			RepositoryLogger.debug("Invalid request: OVERWRITE (" + entity + ")", repoData.getName());
			return null;
		}

		EntityDTO message = new EntityDTO(RequestType.OVERWRITE.getId(), 0, repoData.getName());
		message.getEntities().add(entity);
		SubscribedMessageDTO answer = this.repoClient.sendSubscribedMessage(message);
		RepositoryLogger.debug("The request to overwrite \"" + entity + "\" has been answered with: " + answer,
				repoData.getName());
		return handleAnswer(answer, RequestType.OVERWRITE);
	}

	public void clear() {
		this.repoClient.sendMessage(new EntityDTO(RequestType.CLEAR.getId(), 0, repoData.getName()));
	}

	public EntityClass getByKey(Serializable key) {
		if (key == null) {
			RepositoryLogger.debug("Invalid request: GET (" + key + ", Key)", repoData.getName());
			return null;
		}

		KeyDTO message = new KeyDTO(RequestType.GET.getId(), 0, repoData.getName(), key, -1);
		SubscribedMessageDTO answer = this.repoClient.sendSubscribedMessage(message);
		RepositoryLogger.debug("The request to getByKey \"" + message + "\" has been answered with: " + answer,
				repoData.getName());
		return handleAnswer(answer, RequestType.GET);
	}

	public ArrayList<EntityClass> getByData(Serializable key, String fieldName) {
		if (key == null || fieldName == null || fieldName.trim().equals("")) {
			RepositoryLogger.debug("Invalid request: GET (" + key + ", " + fieldName + ")", repoData.getName());
			return new ArrayList<>();
		}
		int contentId = repoData.getContentId(fieldName.toLowerCase());
		if (contentId < -1) {
			RepositoryLogger.debug("Invalid request: GET (" + key + ", " + fieldName + ") field is unknown",
					repoData.getName());
			return new ArrayList<>();
		}

		KeyDTO message = new KeyDTO(RequestType.GET.getId(), 0, repoData.getName(), key, contentId);
		SubscribedMessageDTO answer = this.repoClient.sendSubscribedMessage(message);
		RepositoryLogger.debug(
				"The request to getByData (\"" + message + "\", " + contentId + ") has been answered with: " + answer,
				repoData.getName());
		return handleMultiAnswer(answer, RequestType.GET);
	}

	public ArrayList<EntityClass> getByData(Serializable key, int fieldId) {
		if (key == null || fieldId < -1 || fieldId > repoData.getData().length - 1) {
			RepositoryLogger.debug("Invalid request: GET (" + key + ", " + fieldId + ")", repoData.getName());
			return new ArrayList<>();
		}

		KeyDTO message = new KeyDTO(RequestType.GET.getId(), 0, repoData.getName(), key, fieldId);
		SubscribedMessageDTO answer = this.repoClient.sendSubscribedMessage(message);
		RepositoryLogger.debug(
				"The request to getByData (\"" + message + "\", " + fieldId + ") has been answered with: " + answer,
				repoData.getName());
		return handleMultiAnswer(answer, RequestType.GET);
	}

	public void removeByKey(Serializable key) {
		if (key == null) {
			RepositoryLogger.debug("Invalid request: REMOVE (" + key + ", Key)", repoData.getName());
			return;
		}

		KeyDTO message = new KeyDTO(RequestType.REMOVE.getId(), 0, repoData.getName(), key, -1);
		this.repoClient.sendMessage(message);
	}

	public void removeByData(Serializable key, String fieldName) {
		if (key == null || fieldName == null || fieldName.trim().equals("")) {
			RepositoryLogger.debug("Invalid request: REMOVE (" + key + ", " + fieldName + ")", repoData.getName());
			return;
		}
		int contentId = repoData.getContentId(fieldName.toLowerCase());
		if (contentId < -1) {
			RepositoryLogger.debug("Invalid request: REMOVE (" + key + ", " + fieldName + ") field is unknown",
					repoData.getName());
			return;
		}

		KeyDTO message = new KeyDTO(RequestType.REMOVE.getId(), 0, repoData.getName(), key, contentId);
		this.repoClient.sendMessage(message);
	}

	public void removeByData(Serializable key, int fieldId) {
		if (key == null || fieldId < -1 || fieldId > repoData.getData().length - 1) {
			RepositoryLogger.debug("Invalid request: REMOVE (" + key + ", " + fieldId + ")", repoData.getName());
			return;
		}

		KeyDTO message = new KeyDTO(RequestType.REMOVE.getId(), 0, repoData.getName(), key, fieldId);
		this.repoClient.sendMessage(message);
	}

	@SuppressWarnings("unchecked")
	private EntityClass handleAnswer(SubscribedMessageDTO answer, RequestType requestType) {
		if (answer.getContent() == null || !(answer.getContent() instanceof EntityDTO)
				|| ((EntityDTO) answer.getContent()).getEntities() == null
				|| ((EntityDTO) answer.getContent()).getEntities().size() != 1) {
			RepositoryLogger.debug("Invalid answer: " + requestType + " (" + answer + ")", repoData.getName());
			return null;
		}

		DTO addedEntity = ((EntityDTO) answer.getContent()).getEntities().get(0);
		try {
			return (EntityClass) addedEntity;
		} catch (ClassCastException e) {
			RepositoryLogger.debug(
					"Invalid answer content: " + requestType + " (" + answer + " - " + addedEntity + ")", e,
					repoData.getName());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private ArrayList<EntityClass> handleMultiAnswer(SubscribedMessageDTO answer, RequestType requestType) {
		if (answer.getContent() == null || !(answer.getContent() instanceof EntityDTO)
				|| ((EntityDTO) answer.getContent()).getEntities() == null) {
			RepositoryLogger.debug("Invalid answer: " + requestType + " (" + answer + ")", repoData.getName());
			return new ArrayList<>();
		}
		EntityDTO dto = ((EntityDTO) answer.getContent());

		ArrayList<EntityClass> entites = new ArrayList<>();
		for(int i = 0; i < dto.getEntities().size(); i++) {
			try {
				entites.add((EntityClass) dto.getEntities().get(i));
			} catch (ClassCastException e) {
				RepositoryLogger.debug(
						"Invalid answer content: " + requestType + " (" + answer + " - " + dto.getEntities().get(i) + ")", e,
						repoData.getName());
			}
		}
		return entites;
	}
}
